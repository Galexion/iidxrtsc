var express = require('express');
var router = express.Router();
var fs = require('fs');
var songs = JSON.parse(fs.readFileSync(__dirname + '/../json/songs-iidx.json'));
var charts = JSON.parse(fs.readFileSync(__dirname + '/../json/charts-iidx.json'));


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'RTSC', fervidex: JSON.parse(fs.readFileSync(__dirname + '/../public/score.json')) });
});

router.post('/:type/:action', function(req,res) {
  let type = req.params.type;
  let action = req.params.action;
  if(type === 'user' || type === 'class') return res.status(400).send('Score Submitions only.');
  let data = req.body;
  let UserAgent = req.header('User-Agent').toString();
  let score = {
    clear: data.clear_type || 0,
    options: {
      gauge: data.option.gauge || "Normal",
      assist: data.option.assist || "None",
      range: data.option.range || "Normal",
      style: data.option.style || "Normal"
    },
    pacemaker: data.pacemaker || "None",
    gauge: data.gauge || ["No Data"],
    chart: data.chart || "Null",
    score: data.ex_score || 0,
    judge: {
      pgreat: data.pgreat || 0,
      great: data.great || 0,
      good: data.good || 0,
      bad: data.bad || 0,
      poor: data.poor || 0,
      fast: data.fast || 0,
      slow: data.slow || 0,
    },
    combo: data.max_combo || 0,
    song_id: data.song_id || 0,
  };

  let diffLetter = data.chart.charAt(data.chart.length -1);
  let difficulty = "";
  let playstyle = "";
  if (data.chart.includes("sp")) {
    playstyle = "SP";
  } else {
    playstyle = "DP";
  }
  if(diffLetter === 'b') {
    difficulty = "BEGINNER";
  } else if (diffLetter === 'n') {
    difficulty = "NORMAL";
  } else if(diffLetter === 'h') {
    difficulty = "HYPER";
  } else if(diffLetter === 'a') {
    difficulty = "ANOTHER";
  }  else {
    res.status(400).send('Invalid Chart');
  }

  const chartMap = new Map();
  for (const chart of charts){
    if (chart.difficulty === difficulty && chart.data.inGameID === data.entry_id && chart.playtype === playstyle) {
      chartMap.set(data.entry_id, chart);
    }
  }
  let chart = chartMap.get(data.entry_id);
  const songMap = new Map();
  for (const song of songs) {
    if(song.id === chart.songID) {
      songMap.set(chart.songID, song);
      console.log(song)
    }
  }
  let song = songMap.get(chart.songID);
  console.log(song);
  let scoreData = {
    song: song,
    chart: chart,
    score: score
  }
fs.writeFile(__dirname + '/../public/' + 'score.json', JSON.stringify(scoreData), function(err) {
  if (err) {
    return console.log(err);
  }
  console.log("The file was saved!");
  res.status(200).send(scoreData);
});
});
module.exports = router;
